#CE Validation

##Description
Allows CodeIgniter form validation callbacks to work in ExpressionEngine templates

##Usage
In your /system/expressionengine/third_party directory, create a folder named *ce_validation* and place the Ce_validation.php file in it.

Inside your php-enabled template:

		<?php
		//load the form validation library like normal
		$this->EE->load->library('form_validation');

		//include the Ce_validation class
		if ( ! class_exists( 'Ce_validation' ) )
		{
			require PATH_THIRD . 'ce_validation/Ce_validation.php';
		}

		//override the original
		$this->EE->form_validation = new Ce_validation();

		//use the library like you normally would
		//------------------- set validation rules -------------------
		//name
		$this->EE->form_validation->set_rules('name', '&ldquo;Name&rdquo;', 'xss_clean|trim|required|callback_valid_name|max_length[50]');

		$this->EE->form_validation->set_message('valid_name', 'The %s field must be a valid name.');

		//------------------- callbacks -------------------
		function valid_name( $str )
		{
			return ( ! preg_match( '#^([a-z\'\- ])+$#i', $str ) ) ? FALSE : TRUE;
		}

Your callbacks will now now work.

Alternatively, you can setup your callbacks as class methods of Ce_validation (which could be helpful if you are planning on using the same callbacks in multiple places throughout your site):

		private function valid_name( $str )
		{
			return ( ! preg_match( '#^([a-z\'\- ])+$#i', $str ) ) ? FALSE : TRUE;
		}
